# Pinecon Console Application
> Pinecon allows you to manage your tasks in the terminal. It is simple, very fast, and stays out of the way. 

Memory like a sieve? Now no need to remember all 
those things that you have to do, because Pinecon will do that for you! 
The application will help you to remind both about important events (birthdays, holidays) 
and about daily routine. You can create simple tasks and plans, group them 
and give them to other users for execution.

![](screenshots/header.jpg)

## Installation

OS X & Linux & Windows:

```sh
pip install https://bitbucket.org/ligonberry/third-python-lab/downloads/pinecon-1.4-py3-none-any.whl
```

## Usage example

__1: Initialize your todos repo and login__

First, run ```pine init``` to intialize the ```.pinecon``` repo in the home directory.

![](screenshots/init.jpg)

![](screenshots/login.jpg)

__2: Add a couple todos__

You can add a task by using the ```pine task add [params]```:

![](screenshots/add_task.jpg)

You can add a plan (periodical task) by using the ```pine plan add [params]```:

![](screenshots/add_plan.jpg)

__3: List todos__

This is the heart of the system. You can show and group your todos.

```pine tasks show```

![](screenshots/show.jpg)

Now, if you show this task, you'll see that it has been finished:

__4: Complete a todo__

Completing a todo is done by using ```pine task close -1```

![](screenshots/close_task.jpg)

Now, if you ```show``` you task, you'll see that it has been executed:

![](screenshots/show_task.jpg)

## Configurations

You can change log file. This file stored in ```.pinecon```

```sh
{
    "level": "DEBUG",
    "format": "[%(levelname)s] %(asctime)s - %(name)s: %(message)s",
    "path": "/home/dubhad/.pinecon/pinelog.log"
}
```

# Pinecon Console Application API

## Installation

OS X & Linux & Windows:

```sh
pip install https://bitbucket.org/ligonberry/third-python-lab/downloads/pinecon_api-0.4-py3-none-any.whl
```

## Usage example

Enter ```python3``` in your shell. 

```sh
>>> import pinecon.library
>>> from pinecon.library.services import service
>>> from pinecon.library.storage import queries
>>> from pinecon.library.storage import storage
>>> connection_string = 'sqlite:///library.db'
>>> data_storage = storage.Storage(connection_string)
>>> queries = queries.Queries(data_storage)
>>> serv = service.Service(queries)
>>> from pinecon.library.models.model.user import User
>>> from pinecon.library.models.model.task import Task
>>> user = serv.add_checked_user(User("Ligonberry"))
>>> task = serv.add_checked_task(Task(user.user_id, "Learning Python"))
```  

If you would like to **log your work with pinecon-api** you should enter:

  ```>>> from pinecon.library.services import loggers```      
  ```>>> loggers.set_logger()```
      
**Note** ```database``` and ```log file``` will create in your **current directory**. But you can change this:  
 
  * for ```database``` by ```connection_string```, 
  * for ```log file``` by calling method ```set_logger``` with params:

  You can add your ```path``` in ```log_path```.

  ```>>> from pinecon.library.services import loggers```
  ```>>> loggers.set_logger(log_path=<your_path>)```
  
  Example: ```loggers.set_logger(log_path='/Users/dasalitvincuk/pinecon.log')```

## Meta

Darya Litvinchuk ```ligonberry@yandex.by```

Distributed under the MIT license. See ``LICENSE`` for more information.

You can also see web application - <https://pinecon.org>

## Contributing

1. Fork it (<https://bitbucket.org/ligonberry/third-python-lab/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

