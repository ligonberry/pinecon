""" This module is intended to test the types that are passed to the parser.

    Classes:
    ---------
        * ArgumentParserException(Exception)
            - class for catching type mismatch errors;

    Methods
    ---------
        * _is_integer(str_: str) - checks whether the input string can be converted to a number;
        * is_positive_integer(str_: str) - checks whether the input string can be converted to a positive number;
        * is_valid_date(str_ : str)
            - checks whether it is possible to represent the entered string in the form of a valid date;
        * is_valid_future_date(str_: str)
            - checks whether it is possible to represent the entered string in the form of a valid future date;
"""
import sys
from datetime import datetime


class ArgumentParserException(Exception):
    """ A common class for all exceptions associated with mismatch types that can occur as a result of parsing. """
    def __init__(self, msg):
        print(f"There is an error in application. Reason: {msg}")
        super(ArgumentParserException, self).__init__(msg)
        sys.exit(2)


def _is_integer(str_: str):
    """ Checks whether the input string can be converted to a number.

    Parameters
    -----------
        * str_ - entered string that we are trying to convert to a number;

    Returns
    -----------
        * integer - number into which we converted the string;

    Raises
    -----------
        * ArgumentParserException - if we could not convert a string to a number;
    """
    try:
        integer = int(str_)
    except ValueError:
        msg = f'not an integer: {str_}.'
        raise ArgumentParserException(msg)
    return integer


def is_positive_integer(str_: str):
    """ Checks whether the input string can be converted to a positive number.

    Parameters
    -----------
        * str_ - entered string that we are trying to convert to a positive number;

    Returns
    -----------
        * integer - positive number into which we converted the string;

    Raises
    -----------
        * ArgumentParserException - if we could not convert a string to a number or
                                    if we could not convert a string to a positive number
    """
    integer = _is_integer(str_)
    if integer > 0:
        return integer
    else:
        msg = f'not a positive integer: {str_}.'
        raise ArgumentParserException(msg)


def is_valid_date(str_: str):
    """ Checks whether it is possible to represent the entered string in the form of a valid date.

    Parameters
    -----------
        * str_ - entered string that we are trying to convert to a valid date;

    Returns
    -----------
        * date: datetime.date - date into which we converted the string;

    Raises
    -----------
        * ArgumentParserException - if we could not convert a string to a valid date;
    """
    try:
        date = datetime.strptime(str_, "%d-%m-%Y")
    except ValueError:
        msg = f'not a valid date: {str_}'
        raise ArgumentParserException(msg)
    return date.date()


def is_valid_future_date(str_):
    """ Checks whether it is possible to represent the entered string in the form of a valid future date.

    Parameters
    -----------
        * str_ - entered string that we are trying to convert to a valid future date;

    Returns
    -----------
        * date: datetime.date - date into which we converted the string;

    Raises
    -----------
        * ArgumentParserException - if we could not convert a string to a valid date or
                                    if we could not convert a string to a valid future date;
    """
    date = is_valid_date(str_)
    if date < datetime.now().date():
        msg = f'not a valid date. The date must(!) be in the future!: {str_}'
        raise ArgumentParserException(msg)
    return date
