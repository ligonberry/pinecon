# coding=utf-8
"""
    This package contains all necessary packages
    to with terminal interface for pinecon application

    Modules:
        * initialization - contains modules related to the primary processing of the result to the user:
                           simple type checks and etc;
        * handling - transmitting data received from the user to functions
                     that are in other modules for further processing
"""