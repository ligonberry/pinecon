""" This module contains all necessary information
    that may be needed when working with the model User."""
from enum import Enum


class User:
    """ Model for a person who uses this application. """

    def __init__(self, name, user_id=None):
        self.name = name
        self.user_id = user_id


class UserAttribute(Enum):
    NAME = "name"

    ID = "user_id"
