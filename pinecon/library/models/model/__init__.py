""" This package provides modules, that contains all necessary information for working with models.

    Modules:
    ----------------
        alert - contains all necessary information that may be needed when working with the model Alert;
        executor_to_task - contains all necessary information,
                           that may be needed when working with the model ExecutorToTaskRelation;
        group - contains all necessary information that may be needed when working with the model Group;
        group_to_task - contains all necessary information,
                        that may be needed when working with the model GroupToTaskRelation
        message - contains all necessary information that may be needed when working with the model Message
        plan - contains all necessary information that may be needed when working with the model Plan
        task - contains all necessary information that may be needed when working with the model Task
        task_date - contains all necessary information that may be needed when working with the model TaskDate
        task_to_task - contains all necessary information,
                       that may be needed when working with the model TaskToTaskRelation
        user - contains all necessary information that may be needed when working with the model User

"""