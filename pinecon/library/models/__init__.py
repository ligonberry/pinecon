""" This package provides modules, to represent abstractions for the pinecon library.

    Modules:
    ----------------
        helpers - contains common things for several models;
        representations - contains extended data models for the representation;

    Packages:
    ----------------
        model - provides modules, that contains all necessary information for working with models

"""