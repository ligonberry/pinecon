import os
from pathlib import Path

PINECON_PATH = os.path.join(str(Path.home()), '.pinecon')

LOG_CONFIG = os.path.join(PINECON_PATH, 'log.conf')

CONNECTION_STRING = 'sqlite:///' + os.path.join(PINECON_PATH, 'pinecon.db')
USER_DATA_FILE = os.path.join(PINECON_PATH, 'user_data.json')
